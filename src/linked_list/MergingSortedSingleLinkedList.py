"""
Two ways to merge sorted linked list
    1. Merge two list by creating a new 3rd list
    2. Merge by rearranging links <- this used in merge sort

Variables used -
StartM -> head reference of Merged List
pM -> Merged List moving pointer

"""

# Merge by creating a new List
# List to be provide should be sorted first

from SingleLinkedListLib import Node


class MergingSortingSingleLinkedList:

    def merge_using_new_list(self, p1, p2):  # p1 and p2 reference to list1 and list2

        # decide the smallest among two list will start node of new list
        if p1.info <= p2.info:
            StartM = Node(p1.info)
            p1 = p1.link
        else:
            StartM = Node(p2.info)
            p2 = p2.link
        pM = StartM

        # iterate and add to new list till one of p1/p2 list reaches None
        while p1 is not None and p2 is not None:
            if p1.info <= p2.info:
                pM.link = Node(p1.info)
                p1 = p1.link
            else:
                pM.link = Node(p2.info)
                p2 = p2.link

            pM = pM.link

        # if 2nd list has finished and elements are left in first list
        # iterate over p1 and keep adding to pM
        while p1 is not None:
            pM.link = Node(p1.info)
            p1 = p1.link
            pM = pM.link

        # if 1st list has finished has finished and elements left in 2nd list
        # iterate over second list and keep adding to pM
        while p2 is not None:
            pM.link = Node(p2.info)
            p2 = p2.link
            pM = pM.link

        return StartM

    # Merging two sorted list by rearranging links
    # Variables used
    #   1. StartM - Reference to merged list using links
    #   2. pM - moving pointer for linking the nodes in two list
    # Merged list will be - inplace merging of two initial list by reforming link
    # After merging if we print using previous list head ref, we get modified list
    #   one starting from least node other starting from second list start ref but,
    #   both ending with same end ref of merged list
    def merge_using_linkig_list(self, p1, p2):
        # by definition list passed  p1 and p2 should  have been sorted earlier
        # If shortest list among p1 and p2 ends, link the last ref of merging list to rest of remaining other list

        # Step 1 - Get the StartM reference, smallest among the two sorted list
        if p1.info <= p2.info:
            StartM = p1
            p1 = p1.link
        else:
            StartM = p2
            p2 = p2.link
        pM = StartM

        # Iterate over rest of p1 and p2 and link till any one reaches None
        while p1 is not None and p2 is not None:
            if p1.info <= p2.info:
                pM.link = p1

                pM = pM.link
                p1 = p1.link
            else:
                pM.link = p2
                pM = pM.link
                p2 = p2.link

        # No need to iterate over remaining list, just link to remaining
        if p1 is None:
            pM.link = p2
        else:
            pM.link = p1

        return StartM


# driver program to check
if __name__ == "__main__":
    from SingleLinkedListLib import SingleLinkedList

    # Check merge_using_new_list
    # Step1 - Create SLL instance
    list1 = SingleLinkedList()
    list2 = SingleLinkedList()

    # Step2 - build each list
    list1.create_list()
    list2.create_list()

    # Step3 - Sort the list using any sorting algo - Using BubbleSort here
    list1.bubble_sort_by_exchanging_data()
    list2.bubble_sort_by_exchanging_data()

    # Display List before MergeSorting
    print("First List - ")
    list1.display_list()

    print("Second List - ")
    list2.display_list()

    # --------------------------------------------------------
    # MergeSort by creating new_list
    merge_list = SingleLinkedList()
    MergingSortingSingleLinkedList_obj01 = MergingSortingSingleLinkedList()
    merge_list.start = MergingSortingSingleLinkedList_obj01.merge_using_new_list(list1.start, list2.start)

    print("Merged List using new list is - ")
    merge_list.display_list()

    # --------------------------------------------------------
    # MergeSort by using linkig_list
    merge_list02 = SingleLinkedList()
    MergingSortingSingleLinkedList_obj02 = MergingSortingSingleLinkedList()
    merge_list02.start = MergingSortingSingleLinkedList_obj02.merge_using_linkig_list(list1.start, list2.start)

    print("Merged list by linking is -")
    merge_list02.display_list()

    # =========================================================
    # Just checking what happened to original list1 and list2 after linking
    print("Just checking what happened to original list1 and list2 after linking")
    print("List 1 - ")
    list1.display_list()
    print("List 2 - ")
    list2.display_list()
