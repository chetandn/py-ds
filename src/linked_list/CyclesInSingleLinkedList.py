'''
This class helps in three operation
    1. Insert Cycle in Single Linked List
    2. Detect Cycle in Single Linked List
    3. Remove Cycle in Single Linked List
'''
class CyclesInSingleLinkedList:

    def __init__(self):
        # print("in CyclesInSingleLinkedList")
        pass

    def insert_cycle_at_node(self, sll_list, search_node_X_info):
        """
        insert a cycle in given linked list at Node search_node_X
        if Node X is not present in List , Display the Node not found
        Algo -
            1. Take two references pPrev and p. 'p' iterates over the loop, 'pPrev' refer one node behind 'p'
            2. While traversing the loop, is you get Search-Node-x , put that ref to 'pX'
            3. When p reached last Node->None, make pPrev point p
        :param sll_list:
        :param x:
        :return:
        """

        # if list is empty
        if sll_list.start is None:
            print('List is empty - returning')
            return

        # List is not empty
        p = sll_list.start  # p ref to iterate over loop
        pPrev = None  # pPrev hold previous ref to 'p'
        pX = None  # gets search Node 'x' reference

        # iterate till last Node to link last node-> None to
        while p is not None:
            if p.info is search_node_X_info:
                pX = p  # store pX reference

            # iterate over p and save its previous reference
            pPrev = p  # previous node ref of p
            p = p.link

        if pX is not None:  # if search node is found
            pPrev.link = pX  # link the pPrev(thats last node of SLL) to pX and form loop
        else :  # if search node is not found
            print(search_node_X_info, "not present in list")

    def detect_cycle(self, sll):
        if self._find_cycle(sll) is None:
            return False
        else:
            return True

    def _find_cycle(self, sll):
        # if no linked list exists or only one node exists
        if sll.start is None or sll.start.link is None:
            return None
        # else its normal SLL with more than two nodes, proceed searching for loops

        slow_ref, fast_ref = sll.start, sll.start

        # use Floyds Cycle detection algo, move slow_ref @ 1x and fast_ref @ 2x
        # so that if both intersect at common node reference, there exists a loop

        # since fast_ref moves faster :) , its bound to reach end of SLL earlier
        # so check for None terminating for fast_ref
        # while fast_ref is not None <- even number of nodes
        # while fast_ref.link is not None <- for odd no of nodes

        while fast_ref is not None and fast_ref.link is not None:

            # keep iterating through loop
            slow_ref = slow_ref.link            # 1x speed
            print('slow_ref', slow_ref.info)
            fast_ref = fast_ref.link.link       # 2x speed
            print('fast_ref', fast_ref.info)

            # if both ref come to common point, break the loop and return ref
            # this if check not before above increments,
            # we are checking after incrementing as at stsrt both ref first node only
            if slow_ref == fast_ref:
                return slow_ref

        # if fast_ref has reached None and above loop terminated, then loop doesn't have loop
        return None

    def remove_cycle(self, sll):
        refs_met_node = self._find_cycle(sll)

        if refs_met_node is None:
            return
        else:
            print('Node at which both ref met is :', refs_met_node.info)

            # initialize both slow_ref and fast_ref @ the point of both ref common overlapping
            # while when cycle detected also they we both present their itself
            slow_ref, fast_ref = refs_met_node, refs_met_node

            # now make them iterate at 1x speed, but slow_ref from start and fast_ref from current overlapping position
            slow_ref = sll.start
            while slow_ref.link != fast_ref.link:
                slow_ref = slow_ref.link
                fast_ref = fast_ref.link

            # now that both point to loop junction node, make fast_ref.link to point None, so loop is avoided
            fast_ref.link = None

# driver program to check
if __name__ == "__main__":
    from SingleLinkedListLib import SingleLinkedList

    # Step1 - Create SLL instance
    list1 = SingleLinkedList()

    # Step2 - build list
    list1.create_list()

    # Display List before
    print("List before inserting cycle - ")
    list1.display_list()

    # create CyclesInSingleLinkedList instance
    CyclesInSingleLinkedList_obj1 = CyclesInSingleLinkedList()
    search_node_X_info = int(input('Enter search_node_X_info to create loop at : '))
    CyclesInSingleLinkedList_obj1.insert_cycle_at_node(list1, search_node_X_info)

    # Display List after
    print("List after inserting cycle - ")
    list1.display_list()
