from SingleLinkedListNode import Node


class SingleLinkedList:

    def __init__(self):
        self.start = None

    def display_list(self):
        if self.start is None:
            print("List is empty")
            return
        else:
            print(" List is : ", end='')

            # initialize the list
            p = self.start
            # traverse the list
            while p is not None:
                # print each node info while traversing
                print(p.info, " ", end='')
                p = p.link
            print()  # print one new line after printing list

    def count_nodes(self):

        p = self.start
        count_of_nodes = 0

        while p is not None:
            count_of_nodes += 1
            p = p.link

        print("Number of nodes in the list = ", count_of_nodes)

    # simple linear search, find at what position search_ele is found
    def search(self, search_ele):
        """
        Search the element.
        :param search_ele: Element passed to search
        :return: If found print position and return True. Else False
        :algo: Pass Condition - Node Found , Failure-condition search_node reached None ie end of list
        """
        position = 1

        p = self.start
        while p is not None:
            if p.info == search_ele:
                print(search_ele, " is at position ", position)
                return True

            position += 1
            p = p.link
        else:  # normally used  to know loop was unsuccessful with in loop data set
            print(search_ele, " was not found in list ")
            return False

    # insert @ beginning
    def insert_at_beggining(self, data):
        # create temporary node
        temp = Node(data)

        # make the links
        temp.link = self.start  # takes care is list is empty
        self.start = temp

    # insert @ end
    def insert_at_end(self, data):
        # create temp node
        temp = Node(data)

        # case 1 - if list is empty
        if self.start is None:
            # link start to new temp node
            self.start = temp
            return

        # case 2 - if ls it is not empty, traverse till last node
        p = self.start
        # how do you know last Node it is? It has thatnode.link == Node
        # till then traverse
        while p.link is not None:
            p = p.link
        # after loop terminates p points to last node, modify its link to point temp node
        p.link = temp

    def create_list(self):
        n = int(input("Enter the number of nodes : "))
        if n == 0:
            return
        for i in range(n):
            data = int(input("Enter the element to be inserted : "))
            self.insert_at_end(data)

    # insert in between the node
    # 3 cases, Insert before a node, after a node and at a given position

    def insert_after(self, new_node_data, insert_after_node_data):

        # Step 1 : Find the node reference of the insert_after_node break
        # Step 2 : If reference found make links

        # Step 1
        p = self.start
        while p is not None:

            if p.info == insert_after_node_data:
                break

            p = p.link

        # step 2
        if p is None:
            print(insert_after_node_data, "is not present in list")
        else:
            temp = Node(new_node_data)
            temp.link = p.link
            p.link = temp

    def insert_before(self, new_node_data, insert_before_node_data):

        # step 1 : checks to be done if list is empty
        # step 2 : check if new node to be added before first node, which involves modifying self.start
        # step 3 : find reference to predecessor of node insert_before_node_data

        # step 1 : if list is empty
        if self.start is None:
            print("List is empty, highly impossible to insert before")
            return

        # step 2:
        if insert_before_node_data == self.start.info:  # cross-check with first node data
            temp = Node(new_node_data)

            temp.link = self.start
            self.start = temp

            return

        # step 3 : find reference to predecessor of node containing X

        # find the reference of the node having insert_before_node_data
        p = self.start
        while p.link is not None:  # aim to check till one node before last node not over that

            if p.link.info == insert_before_node_data:  # check from second node info onwards to last node
                break

            p = p.link

        # if node traverse reference is reached last node( p.link.info check last node info too)
        if p.link is None:
            print(insert_before_node_data, " is not present in the list")
        else:
            temp = Node(new_node_data)
            temp.link = p.link
            p.link = temp

    def insert_at_position(self, new_node_data, position):

        # case 1 : when we want to insert in first position , which needs modifying self.start
        # case 2 : care to be taken to check if requested position is with in list size

        # case 1
        if position == 1:
            temp = Node(new_node_data)
            temp.link = self.start
            self.start = temp
            return

        # case 2
        # initialize
        p = self.start
        predessor_node_count_flag = 1  # start from 1 later, as case 1 covers position 1

        # find reference to (position-1) node and care to be taken such that list is not reached end
        while predessor_node_count_flag < position-1 and p is not None:
            p = p.link
            predessor_node_count_flag += 1

        # if while loop terminated because of reaching end of list, report position is above list size
        if p is None:
            print("You can insert only upto position ", predessor_node_count_flag)
        else:  # successful case
            temp = Node(new_node_data)
            temp.link = p.link
            p.link = temp

    def delete_first_node(self):
        #  case 1 - if list is empty - nothing to delete , just return
        if self.start is None:
            return

        # case 2 - if node exists - make start to point to 1st node link(None or 2nd node reference) of list
        self.start = self.start.link

    def delete_last_node(self):
        #  case 1 - if list is empty - just return
        if self.start is None:
            return

        #  case 2 - if there is only one node, make start refer None and return
        if self.start.link is None:
            self.start = None
            return

        # case 3 - if more than one node exists
        #           find reference to 2nd last node and make its link part to refer None
        p = self.start
        while p.link.link is not None:
            p = p.link

        # after loop terminates, p refer to second last node
        p.link = None

    #  delete a node that contains 'X', delete in between the list
    def delete_node_inbetween(self, delete_node_data):

        #  case 1 - if the list is empty - return
        if self.start is None:
            print("List is empty")
            return

        # case 2 - if exists only node and contains 'delete_node_data'
        if self.start.info == delete_node_data:
            self.start = self.start.link
            return

        # case 3 - Deletion in between or at the end
        p = self.start
        while p.link is not None:

            if p.link.info == delete_node_data:
                break

            p = p.link

        if p.link is None:
            print("Element ", delete_node_data, " not in list")
        else:  # true case
            p.link = p.link.link

    def reverse_list(self):
        # three variables prev, present(p), next
        prev = None
        p = self.start
        while p is not None:
            next = p.link

            # now make the links to refer previous
            p.link = prev

            #move the prev and p forward
            prev = p

            p = next  # p = p.link will loop infinitely

        # at last update start to point last node as after reversing it will be the first
        self.start = prev  # as prev holds last node reference and both next refer None and p copies that

    def bubble_sort_by_exchanging_data(self):
        """
        3 references, p , q and end
        p acts as first node reference
        q acts as second reference
        here 'end' reference acts as marker to stop comparison when p meets end while itterating.
        this end is decrement per pass when each last node gets sorted. thus end updates to p
        Stop decrementing end when it reached 2nd node of list, list is already sorted
        :return:
        """
        end = None  # initialise end to None, virtuallly acts as last node None
        while end != self.start.link :  # check if end points to 2nd Node to stop loop
            p = self.start  # initialize p
            # initialize q i.e next node pointer only when p.link is not pointing end
            while p.link is not end:
                q = p.link

                # compare and swapping
                if p.info > q.info:
                    p.info, q.info = q.info, p.info
                p = p.link  # iterate over next node
            end = p  # after reaching end, last node is 'sorted'. So update 'end' to p last location

    def bubble_sort_by_exchanging_links(self):
        """
        Same use case as above for p q and end.
        need addition r reference which points previous to 'p' reference
        But while compare and swapping follow below steps:
            1. Swap p and q 's link references
                p.link = q.link
                q.link = p
            2. update r link reference to (which was pointing before 'p') point q
                new reversed link flow will be as follows r -> q -> p
            3. Special case when p is at self.start, r will also be pointing to self.start
                So care care to be taken to update self.start to point q now
                self.start = q
            4. Swap p and q so that so p follows q as before swapping and r gets p value before update po p=p.link
                And end is updated as per 'p' per pass
        :return:
        """
        end = None  # mimics points to last node
        # iterate till end is not decremented till 2nd node
        while end != self.start.link:
            # r place holder to save prev reference of p before its updated p=p.link
            r = p = self.start

            # per pass itterate till 'end' marker which will be decremented per pass
            while p.link != end:
                # get q as next reference only when p.link is not end
                q = p.link

                # comparision and swapping
                if p.info > q.info:
                    # update 'r -> p -> q' to 'r -> q -> p'
                    p.link = q.link
                    q.link = p

                    if p != self.start:
                        r.link = q
                    else:
                        # special case @ beginning when both p and r points to self.start
                        # need to update to 'self.start -> q -> p'
                        self.start = q

                    # swap the reference so that updated 'p -> q' to 'q -> p' to 'p -> q' again ;
                    p, q = q, p

                # update previous before updating p
                r = p
                p = p.link

            # update end after each pass as last node is soted per pass
            end = p

    " merge sort using functionality from merge_using_linkig_list"
    def merge_sort(self):
        """
        It uses recursion to divide the list and join the list.
        Algorithm is
            1. Recursively divide the given list into two halves till, single element is left
            so that it cannot be finally further divided. Base Condition one or zero element after dividing
            2. Recursively join these single elements by linking the nodes in ascending order

        :return: StartM of merged list
        """
        self.start = self._merge_sort_recursively(self.start)

    def _merge_sort_recursively(self, lst_start): # calling this recursively

        # Base Condition - if list is empty or has one element
        if lst_start is None or lst_start.link is None:
            return lst_start

        # if > one element in list call _divide_list recursively
        # here _divide_list provide the reference from other half of current list by setting None @ end of current list
        # start_first_half store current list start
        # start_second_half store next half list start

        start_of_first_half = lst_start
        start_of_second_half = self._divide_list(lst_start)  # divide the list and return start_of_second_half reference

        # within start_of_first_half recursively divide (8nodes->left 4nodes->left 2 nodes)
        start_of_first_half = self._merge_sort_recursively(start_of_first_half)
        # with in start_of_second_half recursively divide (8 nodes-> right 4 nodes -> right 2 nodes)
        start_of_second_half = self._merge_sort_recursively(start_of_second_half)

        # this below line ( merge_using_linkig_list() )executes for first time when base condition of single node is left
        # this will merge two nodes in ascending order
        # later in subsequent calls it merges two lists in ascending order
        from MergingSortedSingleLinkedList import MergingSortingSingleLinkedList
        MergingSortingSingleLinkedList_obj01 = MergingSortingSingleLinkedList()
        StartM = MergingSortingSingleLinkedList_obj01.merge_using_linkig_list(start_of_first_half, start_of_second_half)
        return StartM

    def _divide_list(self, p):
        """
        used to divide list and provide the reference to second half of list and
        set first half last node to None
        Algorithm:
            q is fast pointer - 2x
            p is slow pointer - 1x

        Either when q reached None (when even number of nodes are present) or
                q.link reached None (when odd number of nodes is present)
                save p.link , which gives start_of_second_half
                then sent p.link -> note which terminates first list @ half position of orginal list

        :param p: original list start reference, which list needs to be divided
        :return:
        """
        q = p.link.link
        while q is not None and q.link is not None:
            # iterate till then
            p = p.link  # 1x speed - Tortoise
            q = q.link.link  # 2x speed - Hare

        # when loop is terminated, q or q.link has reached None
        # save p.link , which gives start_of_second_half
        # then sent p.link -> note which terminates first list @ half position of original list
        start_of_second_half = p.link
        p.link = None
        return start_of_second_half

        
# Driver program
if __name__ == '__main__':

    list1 = SingleLinkedList()
    list1.create_list()

    while True:
        print()
        print("01 - Display List")
        print("02 - Count the no. of nodes")
        print("03 - Search for an element")

        print("04 - Insert in empty list \\ Insert in beginning of the list")
        print("05 - Insert a node at the end of the list")
        print("06 - Insert a node after a Specified node")
        print("07 - Insert a node before a Specified node")
        print("08 - Insert a node at a given position")

        print("09 - Delete first node")
        print("10 - Delete last node")
        print("11 - Delete any node")

        print("12 - Reverse the list")

        print("13 - Bubble sort by exchanging data")
        print("14 - Bubble sort by exchanging links")
        print("15 - Merge Sort")

        print("16 - Insert Cycle")
        print("17 - Detect Cycle")
        print("18 - Remove Cycle")
        print("19 - Quit")

        option = int(input('Enter your choice: '))

        if option == 1:
            list1.display_list()
        elif option == 2:
            list1.count_nodes()
        elif option == 3:
            search_ele = int(input("Enter the ele to be searched :"))
            list1.search(search_ele)
        elif option == 4:
            insert_ele = int(input("Enter the element to be inserted at beginning: "))
            list1.insert_at_beggining(insert_ele)
        elif option == 5:
            insert_ele = int(input("Enter the element to be inserted at end: "))
            list1.insert_at_end(insert_ele)
        elif option == 6:
            insert_ele = int(input("Enter the element to be inserted after reference node: "))
            reference_ele = int(input("Enter the reference element after which insert_ele to be placed: "))
            list1.insert_after(insert_ele, reference_ele)
        elif option == 7:
            insert_ele = int(input("Enter the element to be inserted before reference node: "))
            reference_ele = int(input("Enter the reference element before which insert_ele to be placed: "))
            list1.insert_before(insert_ele, reference_ele)
        elif option == 8:
            insert_ele = int(input("Enter the element to be inserted at given position: "))
            position = int(input("Enter the position at which insert_ele to be placed: "))
            list1.insert_at_position(insert_ele, position)
        elif option == 9:
            list1.delete_first_node()
        elif option == 10:
            list1.delete_last_node()
        elif option == 11:
            delete_node_data = int(input("Enter the element data to be deleted: "))
            list1.delete_node_inbetween(delete_node_data)
        elif option == 12:
            list1.reverse_list()
        elif option == 13:
            list1.bubble_sort_by_exchanging_data()
        elif option == 14:
            list1.bubble_sort_by_exchanging_links()
        elif option == 15:
            list1.merge_sort()
        elif option == 16:
            from CyclesInSingleLinkedList import CyclesInSingleLinkedList
            CyclesInSingleLinkedList_obj = CyclesInSingleLinkedList()
            cycle_node_info = int(input('Enter node value at which cycle to be created :'))
            CyclesInSingleLinkedList_obj.insert_cycle_at_node(list1, cycle_node_info)
        elif option == 17:
            from CyclesInSingleLinkedList import CyclesInSingleLinkedList
            CyclesInSingleLinkedList_obj = CyclesInSingleLinkedList()
            if CyclesInSingleLinkedList_obj.detect_cycle(list1):
                print("Cycle detected")
            else:
                print("Cycle not detected in list")
        elif option == 18:
            from CyclesInSingleLinkedList import CyclesInSingleLinkedList
            CyclesInSingleLinkedList_obj = CyclesInSingleLinkedList()
            CyclesInSingleLinkedList_obj.remove_cycle(list1)
        elif option == 19:
            break
        else:
            print("Wrong Option")
            print()
