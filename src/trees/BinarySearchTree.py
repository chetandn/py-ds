"""
A binary search tree is that it is either
    i. Empty or
    ii. Has the following properties
        a. All the keys in left subtree of root are < the key in the root
        b. All the keys in right subtree of root are > the key in the root
        c. Left and Right subtree of root are also binary search trees

                                     70
                                 //       \\
                                //         \\
                              40            80
                           //    \\       //  \\
                          35     50     75      89
                        //  \\     \\	      //  \\
                      30     37     55      82     93
"""


class TreeEmptyError(Exception):
    pass


class Node:
    def __init__(self, data):
        self.info = data
        self.lChild = None
        self.rChild = None


class BinarySearchTree:
    def __init__(self):
        self.root = None

    def is_empty(self):
        return self.root is None

    def insert(self, data_to_insert):
        self.root = self._insert(self.root, data_to_insert)

    def _insert(self, parse_node_ref, data_to_insert):
        if parse_node_ref is None:
            parse_node_ref = Node(data_to_insert)
        elif data_to_insert < parse_node_ref.info:
            parse_node_ref.lChild = self._insert(parse_node_ref.lChild, data_to_insert)
        elif data_to_insert > parse_node_ref.info:
            parse_node_ref.rChild = self._insert(parse_node_ref.rChild, data_to_insert)
        else:
            print(data_to_insert, " already present in the tree")
        return parse_node_ref

    def search(self, data_to_search):
        return self._search(self.root, data_to_search) is not None

    def _search(self, parse_node_ref, data_to_search):
        if parse_node_ref is None:
            return None  # Key not found
        if data_to_search < parse_node_ref.info:  # if value is greater , search in left sub tree
            return self._search(parse_node_ref.lChild, data_to_search)
        if data_to_search > parse_node_ref.info:  # if  value is smaller search in right sub tree
            return self._search(parse_node_ref.rChild, data_to_search)
        return parse_node_ref  # key found

    def delete(self, data_to_delete):
        """
        Deletion of a node in Binary Search Tree :
            3 cases when a given node to be deleted
            case 1 : Node has no child, its a leaf node
            case 2 : Node has exactly one child
            case 3 : Node has exactly two children or subtree

                Parent-PR   <- previous recursive call
                /
            Present Node to delete-P  <- current recursive call context
            /
        Child node - CH  <- next recursive call


                ps - parent of inorder successor   or      ps
                /                                          /
            s - Inorder Successor                        s
                                                          \
                                                          ch

        After copy data of inorder succesor to P
        move PR -> ps
        move P -> s

        delete using case 1 or case 2

                                70
                           /           \
                         30             84
                        /  \\          /   \\
                      12    45        78    95
                     /     /  \\      /     /
                    9     38   60   72     86
                          /          \
                         35          73

        # Inorder successor of N : Leftmost node in the right subtree of N
        Inorder successor of 30 -> 35
        Inorder successor of 70 -> 72

        Inoder Successor of Node will either have
            - No Child or
            - Only right child
        because its a leftmost Node

        :param data_to_delete:
        :return:
        """
        self.root = self._delete(self.root, data_to_delete)

    def _delete(self, parse_node_ref, data_to_delete):

        # Actually deleting process is
        #   1. Find the Inorder(Right sub tress left most ele) successor of node to be deleted
        #   2. Copy data of Inorder Successor node to current deleting Node
        #   3. Actaully delete Inorder successor node by linking its parent-node-link to
        #       Inoder-successor to 'None' or Inorder-successor's-right node link
        #       Since itself is left most node it doesn't has left node child.

        # Search the key
        if parse_node_ref is None:
            print(data_to_delete, "not found")
            return parse_node_ref

        if data_to_delete < parse_node_ref.info:  # search from left sub tree and delete
            parse_node_ref.lChild = self._delete(parse_node_ref.lChild, data_to_delete)
        elif data_to_delete > parse_node_ref.info:  # search from right subtree and delete
            parse_node_ref.rChild = self._delete(parse_node_ref.rChild, data_to_delete)
        else:  # key to be deleted is found
            # case 3 : Node has exactly two children or subtree
            # find the inorder successor of the node to be deleted
            # copy the data of the Inorder successor to the Node
            # Delete the inorder succesor from tne tree
            #   this inorder successor to be deleted has either no child or right child
            #   it can be deleted using Case 1 or Case 2
            if parse_node_ref.lChild is not None and parse_node_ref.rChild is not None:
                # 2 children

                # finding the Inorder successor- Search by moving to right sub tree and left most node
                s = parse_node_ref.rChild
                while s.lChild is not None:
                    s = s.lChild

                # when Inorder Succesor is found copy successor.data to current node
                parse_node_ref.info = s.info

                # delete the Inorder successor node
                # i.e get the None or InorderSuccessor.rChild reference and assign to current node rChild
                parse_node_ref.rChild = self._delete(parse_node_ref.rChild, s.info)
                # above parse_node_ref is parent of search node
                # next recursive call _delete() holds child node (to search node)
                # _delete() return its child ref rRight, which will be assigned to parent above parse_node_ref.rChild
            else:  # 1 child or no child <- else condition will only execute for deleting Inorder Succ Node
                # during unbalanced leaf nodes
                if parse_node_ref.lChild is not None:
                    ''' the complete tree is just as below
                        parent
                        /
                    child                    
                    '''
                    # special case only left child in complete tree
                    # it will fail during inorder successor searching as parse_node_ref.lChild always reach  None
                    ch = parse_node_ref.lChild
                else:  # only right child / no child ( applies both in searching complete tree / inorder successor)
                    ch = parse_node_ref.rChild

                parse_node_ref = ch

        return parse_node_ref

    # find minimum element recursively
    def min(self):

        # if tree is empty
        if self.is_empty():
            raise TreeEmptyError("Tree is empty")

        # find and return min element
        return self._min(self.root).info

    def _min(self, parse_node_ref):
        # Minimum value node in BST is Last Node in Left Most path starting from root

        # base condition when loop to stop
        if parse_node_ref.lChild is None:
            return parse_node_ref

        # recursive call
        return self._min(parse_node_ref.lChild)

    def max(self):

        # if tree is empty raise exception
        if self.is_empty():
            raise TreeEmptyError("Tree is empty")

        return self._max(self.root).info

    def _max(self, parse_node_ref):
        """
        max element in tree is RightMost Node in Right Sub tree
        :param parse_node_ref:
        :return:
        """

        # base condition
        if parse_node_ref.rChild is None:
            return parse_node_ref

        # call recursively
        if parse_node_ref.rChild is None:
            return parse_node_ref

        return self._max(parse_node_ref.rChild)

    def display(self):
        self._display(self.root, 0)  # call from 0th level to display tree
        print()

    def _display(self, parse_node_ref, level_of_tree):

        if parse_node_ref is None:
            return

        self._display(parse_node_ref.rChild, level_of_tree + 1)
        print()

        for i in range(level_of_tree):
            print(" ", end='')
        print(parse_node_ref.info)

        self._display(parse_node_ref.lChild, level_of_tree+1)

    # print pre order - +LR : Root -> Left Sub Tree -> Right Sub Tree
    def preorder(self):
        self._preorder(self.root)
        print()

    def _preorder(self, parse_node_ref):
        if parse_node_ref is None:
            return

        print(parse_node_ref.info, end=" ")
        self._preorder(parse_node_ref.lChild)
        self._preorder(parse_node_ref.rChild)

    # print in order - L+R : Left Sub Tree -> Root -> Right Sub Tree
    def inorder(self):
        self._inorder(self.root)
        print()

    def _inorder(self, parse_node_ref):

        if parse_node_ref is None:
            return

        self._inorder(parse_node_ref.lChild)
        print(parse_node_ref.info, end=" ")
        self._inorder(parse_node_ref.rChild)

    # print post order - LR+ : Left Sub Tree -> Right Sub Tree -> Root
    def postorder(self):
        self._postorder(self.root)
        print()

    def _postorder(self, parse_node_ref):

        if parse_node_ref is None:
            return

        self._postorder(parse_node_ref.lChild)
        self._postorder(parse_node_ref.rChild)
        print(parse_node_ref.info, end=" ")

    def height(self):
        return self._height(self.root)

    def _height(self, parse_node_ref):

        if parse_node_ref is None:
            return 0

        height_of_left_sub_tree = self._height(parse_node_ref.lChild)
        height_of_right_sub_tree = self._height(parse_node_ref.rChild)

        if height_of_left_sub_tree > height_of_right_sub_tree:
            return 1 + height_of_left_sub_tree
        else:
            return 1 + height_of_right_sub_tree


if __name__ == '__main__':

    bst1 = BinarySearchTree()
    bst1.insert('50')
    bst1.insert('33')
    bst1.insert('77')
    bst1.insert('22')
    bst1.insert('88')
    bst1.insert('99')
    bst1.insert('60')
    bst1.insert('51')

    # print(bst1.search('50'))
    bst1.delete('50')
    # print(bst1.search('50'))

    bst1.display()

    print("preorder:")
    bst1.preorder()

    print("in order")
    bst1.inorder()

    print("post order:")
    bst1.postorder()

    print("height of tree:", bst1.height())