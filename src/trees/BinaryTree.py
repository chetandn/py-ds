from TreeNode import Node

class BinaryTree:

    def __init__(self):
        self.root = None  # basic property of tree

    def is_empty(self):
        return self.root == None

    def display(self):
        self._display(self.root, 0)  # zero level @ root
        print()

    # recursive function
    def _display(self, p, level):

        if p is None:
            return

        self._display(p.rChild, level+1)
        # print()

        for i in range(level):
            print(" ", end="")

        print(p.info)

        self._display(p.lChild, level+1)

    # Dummy create tree
    def create_tree(self):
        """
                    P                 =>                        =>       
                    |                 =>            |-None      =>       
             ---------------          =>        |-R-|           =>        R
             |             |          =>        |   |-X         =>         X
             Q             R          =>      P-|               =>       P
             |             |          =>        |   |-B         =>         B
          --------      --------      =>        |-Q-|           =>        Q
          |      |      |      |      =>            |-A         =>         A
          A      B      X     None    =>              	        =>       

        :return:
        """
        self.root = Node('P')
        self.root.lChild = Node('Q')
        self.root.rChild = Node('R')

        self.root.lChild.lChild = Node('A')
        self.root.lChild.rChild = Node('B')

        self.root.rChild.lChild = Node('X')

    def preorder(self):
        self._preorder(self.root)
        print()

    def _preorder(self, p):
        if p is None:
            return
        print(p.info, " ", end='')
        self._preorder(p.lChild)
        self._preorder(p.rChild)

    def inorder(self):
        self._inorder(self.root)
        print()

    def _inorder(self, p):
        if p is None:
            return
        self._inorder(p.lChild)
        print(p.info, " ", end='')
        self._inorder(p.rChild)

    def postorder(self):
        self._postorder(self.root)
        print()

    def _postorder(self, p):
        if p is None:
            return
        self._postorder(p.lChild)
        self._postorder(p.rChild)
        print(p.info, " ", end='')

    def level_order(self):
        if self.root is None:
            print("Tree is Empty")
            return

        from collections import deque
        qu = deque()
        qu.append(self.root)

        while len(qu) != 0:

            p = qu.popleft()
            print(p.info, " ", end='')

            if p.lChild is not None:
                qu.append(p.lChild)
            if p.rChild is not None:
                qu.append(p.rChild)

    # Function to  print level order traversal of tree
    def printLevelOrder(self, root):
        h = self.height()
        for i in range(1, h + 1):
            print('\nLevel :', i)
            self.printGivenLevel(root, i)

            # Print nodes at a given level

    def printGivenLevel(self, root, level):
        if root is None:
            return
        if level == 1:
            print(root.info, end='')
        elif level > 1:
            # to print level 2, reduce level (2-1) time also root parses by (2-1)time level down to second level
            self.printGivenLevel(root.lChild, level - 1)
            self.printGivenLevel(root.rChild, level - 1)

    def height(self):
        return self._height(self.root)

    def _height(self, p):
        if p is None:
            return 0

        hL = self._height(p.lChild)
        hR = self._height(p.rChild)

        if hL > hR:
            return 1 + hL
        else:
            return 1 + hR


# testing the binary tree
if __name__ == '__main__':
    bt_obj1 = BinaryTree()

    bt_obj1.create_tree()

    print("\nDefault tree created is : \n")
    bt_obj1.display()
    print()

    print("Preorder :")
    bt_obj1.preorder()
    print()

    print("Postorder :")
    bt_obj1.postorder()
    print()

    print("Level Order : ")
    bt_obj1.level_order()
    print()

    print("Level Order Method 2: ")
    bt_obj1.printLevelOrder(bt_obj1.root)
    print()

    print("Height of tree is : ", bt_obj1.height())
