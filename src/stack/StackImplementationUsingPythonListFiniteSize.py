class StackEmptyError(Exception):
    pass

# Need exception if stack is full also
class StackFullError(Exception):
    pass

class Stack:

    # initialize the stack with finite size
    def __init__(self, max_size=10):
        self.items = [None] * max_size
        # maintain a counter to know overflowing or underflow as stack size now is limited
        self.count = 0

    def size(self):
        return self.count

    def is_empty(self):
        return self.count == 0

    def is_full(self):
        return self.count == len(self.items)

    def push(self, ele):
        # check stack is not overflowing before pushing
        if self.is_full():
            raise StackFullError("Stack is full, cant push")

        # else push the data @ count and increment count
        self.items[self.count] = ele
        self.count += 1

    def pop(self):
        # check if stack is not underflow
        if self.is_empty():
            raise StackEmptyError("Stack is empty, cant pop")

        # pop @ count-1 and then decrement count
        ele = self.items[self.count - 1]

        # initialize None at popped location
        self.items[self.count - 1] = None

        self.count -= 1

        return ele

    def peek(self):
        if self.is_empty():
            raise StackEmptyError("Stack is empty, cant peek")

        return self.items[self.count - 1]

    def display(self):
        print(self.items)

## Driver program
if __name__ == "__main__":

    stk2 = Stack(20) # override 10 and crate for 20 elements

    while True:
        print("1. Push")
        print("2. Pop")
        print("3. Peek")
        print("4. Size")
        print("5. Display")
        print("6. Quit")

        choice = int(input("enter your choice :"))

        if choice == 1:
            ele = int(input("Enter the element to be pushed : "))
            stk2.push(ele)
        elif choice == 2:
            ele = stk2.pop()
            print("Popped element is : ", ele)
        elif choice == 3:
            print("Element at the top is : ", stk2.peek())
        elif choice == 4:
            print("Size of stack ", stk2.size())
        elif choice == 5:
            stk2.display()
        elif choice == 6:
            break
        else:
            print("Wrong choice entered")
        print()