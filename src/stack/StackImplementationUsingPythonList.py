class EmptyStackError(Exception):
    pass

class Stack:

    # initialize python empty list for stack implementation
    def __init__(self):
        self.items = []

    def is_empty(self):
        # return len(self.items) == 0
        return self.items == []

    def size(self):
        return len(self.items)

    def push(self, element):
        # no need to check stack full as python list dynamically grows
        self.items.append(element)

    def pop(self):
        # check if stack/python list is not empty before popping
        if self.is_empty():
            raise EmptyStackError("Stack is Empty")
        return self.items.pop()  # use python lists default pop()

    def peek(self):
        # check stack/ python list is not empty before peeking
        if self.is_empty():
            raise EmptyStackError("Stack is Empty")
        return self.items[-1]  # use python -ve indexing to find last ele in list

    def display(self):
        print(self.items)  # print the whole list

## Driver program
if __name__ == "__main__":

    # create a stack object
    stk1 = Stack()

    while True:
        print("1. Push")
        print("2. Pop")
        print("3. Peek")
        print("4. Size")
        print("5. Display")
        print("6. Quit")

        choice = int(input("Enter your choice : "))

        if choice == 1:
            ele = int(input("Enter the element to  be pushed : "))
            stk1.push(ele)
        elif choice == 2:
            ele = stk1.pop()
            print("Element Popped is : ", ele)
        elif choice == 3 :
            print("Element at top is : ", stk1.peek())
        elif choice == 4:
            print("Size of the stack ", stk1.size())
        elif choice == 5:
            stk1.display()
        elif choice == 6:
            break
        else:
            print("Invalid choice entered")
        print()


