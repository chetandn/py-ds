from DoubleLinkedListNode import Node


class DoubleLinkedList:

    def __init__(self):
        self.start = None

    def display_list(self):

        if self.start is None:
            print("List is empty")
            return
        else:
            print(" List is : ", end='')

            # initialize the list
            p = self.start
            # traverse the list
            while p is not None:
                # print each node info while traversing
                print(p.info, " ", end='')
                p = p.next
            print()  # print one new line after printing list

    def count_nodes(self):

        p = self.start
        count_of_nodes = 0

        while p is not None:
            count_of_nodes += 1
            p = p.next

        print("Number of nodes in the list = ", count_of_nodes)

    # simple linear search, find at what position search_ele is found
    def search(self, search_ele):
        """
        Search the element.
        :param search_ele: Element passed to search
        :return: If found print position and return True. Else False
        :algo: Pass Condition - Node Found , Failure-condition search_node reached None ie end of list
        """
        position = 1

        p = self.start
        while p is not None:
            if p.info == search_ele:
                print(search_ele, " is at position ", position)
                return True

            position += 1
            p = p.next
        else:  # normally used  to know loop was unsuccessful with in loop data set
            print(search_ele, " was not found in list ")
            return False

    # insert in empty list
    def insert_in_empty_list(self, data):
        temp = Node(data)
        self.start = temp

    # insert @ beginning
    def insert_at_beggining(self, data):
        # create temporary node
        temp = Node(data)

        # make the links
        temp.next = self.start  # takes care is list is empty
        # temp.prev = None # not required , as Node __init__ already initalizes it to None
        self.start.prev = temp
        self.start = temp

    # insert @ end
    def insert_at_end(self, data):
        # create temp node
        temp = Node(data)

        # case 1 - if list is empty
        if self.start is None:
            # link start to new temp node
            self.start = temp
            return

        # case 2 - if list it is not empty, traverse till last node
        p = self.start
        # how do you know last Node it is? It has thatnode.next == None
        # till then traverse
        while p.next is not None:
            p = p.next
        # after loop terminates p points to last node, modify its link to point temp node
        p.next = temp
        temp.prev = p

    def create_list(self):
        n = int(input("Enter the number of nodes : "))
        if n == 0:
            return
        for i in range(n):
            data = int(input("Enter the element to be inserted : "))
            self.insert_at_end(data)

    def insert_after(self, data, x):
        """
        Insert the node_with_data after key_node_data x
        :param data:
        :param x:
        :return:
        """
        # Step 1: Create Node with data
        temp_node = Node(data)

        # Step 2: Iterate over DLL till you get Node X
        p = self.start
        while p is not None:
            if p.info == x:
                break
            p = p.next

        # Step 3: If node_x found insrt after , else return printing
        if p is None:
            print("%s is not present in list" % x)
        else:
            temp_node.prev = p
            temp_node.next = p.next
            # make next_node_prev to ref temp node, care to be taken if X data node itself is last node
            # if p.next is not None:  # <- Cross check later
            if p.next.prev is not None:
                p.next.prev = temp_node
            p.next = temp_node

    def insert_before(self, new_node_data: int, search_node_data_x: int) -> None:
        """
        Insert the node_with_data before search key_node_data x
        Care to be taken while the search node is 1st element which involves modifying self.start
        """

        # Case 1 : if the list is empty -> return
        if self.start is None:
            print('List is empty')
            return

        # Case 2 : if search node is found at 1st position, need to insert @ the beginning of list
        if self.start.info == search_node_data_x:
            temp = Node(new_node_data)

            temp.next = self.start
            self.start.prev = temp
            self.start = temp

            return

        # Case 3 : General case, itterate over DLL in search of search_node_data and insert

        # i. Search
        p = self.start
        while p is not None:
            if p.info == search_node_data_x:
                break

            p = p.next

        # ii. insert if found else return message "List empty"
        if p is None:
            print(search_node_data_x, "Not present in List")
        else:
            temp = Node(new_node_data)

            temp.prev = p.prev
            temp.next = p
            p.prev.next = temp
            p.prev = temp

    def delete_first_node(self):
        # case 1 : if list is empty -> return
        if self.start is None:
            return

        # case 2 : if DLL has only one node , make start to None
        if self.start.next is None:
            self.start = None
            return

        # case 3 : make start to point next node and next node prev to None
        self.start = self.start.next
        # currently self.start is referring to next node and operattes on next node
        self.start.prev = None

    def delete_last_node(self):
        # if list is empty -> return
        if self.start is None:
            return

        # if list has only one node, set start to None
        if self.start.next is None:
            self.start = None
            return

        # else get to last node and set from previous to last node next to None
        p = self.start
        while p.next is not None:
            p = p.next

        # when you are at last node
        p.prev.next = None

    def delete_node_inbetween(self, reference_node_to_delete):

        # case 1: when list is empty -> return
        if self.start is None:
            return

        # case 2: if list has one node
        #   i. And if that node is reference_node_to_delete
        #   ii. and is that node is not reference_node_to_delete3

        if self.start.next is None:
            if self.start.info == reference_node_to_delete:
                self.start = None
            else:
                print(reference_node_to_delete, " is not found in list")

        # case 3: if  reference_node_to_delete is found in first node of bigger DLL
        #           it involves modifying of self.start
        if self.start.info == reference_node_to_delete:
            self.start = self.start.next
            self.start.prev = None
            return

        # case 4 : if reference_node_to_delete is found elsewhere than first node in bigger DLL
        #   4.i : Node found at elsewhere other than last and first node i.e in between
        #   4.ii : Node found at last node

        # iterate to get reference_node_to_delete
        p = self.start.next
        while p.next is not None:

            if p.info == reference_node_to_delete:
                break

            p = p.next

        # 4.i
        if p.next is not None:
            p.prev.next = p.next
            p.next.prev = p.prev
        else :  # 4.ii
            if p.info == reference_node_to_delete:
                p.prev.next = None
            else:
                print(reference_node_to_delete," is not found")

    def reverse_list(self):  # this uses 3 ref variables, check below for reversing using two ref

        # if list is empty
        if self.start is None:
            return

        behind_node_ref = None
        ahead_node_ref = self.start
        current_node_ref = self.start

        while current_node_ref is not None:

            # update ahead_node_ref before and hold next node ref before modifying current node references
            ahead_node_ref = current_node_ref.next

            # reversion the direction of node references in current node
            current_node_ref.next = behind_node_ref
            current_node_ref.prev = ahead_node_ref

            # update behind_node_ref
            behind_node_ref = current_node_ref
            # update current_node_ref for next iteration
            current_node_ref = ahead_node_ref

        self.start = behind_node_ref

    def reverse_list_using_two_ref(self):

        if self.start is None:
            return

        behind_ref_node = self.start
        ahead_ref_node = behind_ref_node.next

        # modify the first node of list
        behind_ref_node.next = None
        behind_ref_node.prev = ahead_ref_node

        # iterate over
        while ahead_ref_node is not None:

            # just copy replace node prev with next
            ahead_ref_node.prev = ahead_ref_node.next
            ahead_ref_node.next = behind_ref_node

            # update ahead and behind nodes
            behind_ref_node = ahead_ref_node
            ahead_ref_node = ahead_ref_node.prev # as prev stores next node data in above step

        # after loop terminates, update self.start to point last node
        self.start = behind_ref_node  # as ahead ref is -> None


# Driver program
if __name__ == '__main__':

    dll_list1 = DoubleLinkedList()
    dll_list1.create_list()

    while True:
        print()
        print("01 - Display List")
        print("02 - Count the no. of nodes")
        print("03 - Search for an element")

        print("04 - Insert in empty list \\ Insert in beginning of the list")
        print("05 - Insert a node at the end of the list")
        print("06 - Insert a node after a Specified node")
        print("07 - Insert a node before a Specified node")

        print("08 - Delete first node")
        print("09 - Delete last node")
        print("10 - Delete any node")

        print("11 - Reverse the list")
        print("12 - Reverse the list using two variables")

        print("13 - Quit")

        option = int(input('Enter your choice: '))

        if option == 1:
            dll_list1.display_list()
        elif option == 2:
            dll_list1.count_nodes()
        elif option == 3:
            search_ele = int(input("Enter the ele to be searched :"))
            dll_list1.search(search_ele)
        elif option == 4:
            insert_ele = int(input("Enter the element to be inserted at beginning: "))
            dll_list1.insert_at_beggining(insert_ele)
        elif option == 5:
            insert_ele = int(input("Enter the element to be inserted at end: "))
            dll_list1.insert_at_end(insert_ele)
        elif option == 6:
            insert_ele = int(input("Enter the element to be inserted after reference node: "))
            reference_ele = int(input("Enter the reference element after which insert_ele to be placed: "))
            dll_list1.insert_after(insert_ele, reference_ele)
        elif option == 7:
            insert_ele = int(input("Enter the element to be inserted before reference node: "))
            reference_ele = int(input("Enter the reference element before which insert_ele to be placed: "))
            dll_list1.insert_before(insert_ele, reference_ele)
        elif option == 8:
            dll_list1.delete_first_node()
        elif option == 9:
            dll_list1.delete_last_node()
        elif option == 10:
            delete_node_data = int(input("Enter the element data to be deleted: "))
            dll_list1.delete_node_inbetween(delete_node_data)
        elif option == 11:
            dll_list1.reverse_list()
        elif option == 12:
            dll_list1.reverse_list_using_two_ref()
        elif option == 13:
            break
        else:
            print("Wrong Option")
            print()
