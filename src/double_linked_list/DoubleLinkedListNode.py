class Node:

    def __init__(self, data):
        self.prev = None
        self.info = data
        self.next = None
