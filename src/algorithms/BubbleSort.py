def bubble_sort(arr_list):
    for i in range(len(arr_list)-1, 0, -1):
        for j in range(i):
            if arr_list[j] > arr_list[j+1]:
                arr_list[j], arr_list[j+1] = arr_list[j+1], arr_list[j]  # Swapping


# Driver program
list_1 = [6, 3, 1, 5, 9, 8]
print('Before :', list_1)
bubble_sort(list_1)
print('After  :', list_1)
